# hse-logger

A OOP based logging system inspired by the Phython logging module.

For more information and a quick introduction have a look at our official website: https://dokuwiki.hampel-soft.com/code/open-source/hse-logger

## :rocket: Installation

Download the latest VI Package at https://www.vipm.io/package/hse_lib_hse_logger and install it globally
with the [VI Package Manager](https://www.vipm.io/download/). 

If you want to use the HSE-Logger only in a project scope, clone the repository and build a Source Distribution.
Then you can copy the `hse-logger.lvlib` into your project.


### Configuration 

No configuration needed.


## :bulb: Usage

See example files in folder `Examples`.


## :busts_in_silhouette: Contributing 

We welcome every and any contribution. On our Dokuwiki, we compiled detailed information on 
[how to contribute](https://dokuwiki.hampel-soft.com/processes/collaboration). 
Please get in touch at (office@hampel-soft.com) for any questions.


## :beers: Credits

* Manuel Sebald
* Joerg Hampel

## :page_facing_up: License 

This project is licensed under a modified BSD License - see the [LICENSE](LICENSE) file for details